from asyncio.windows_events import NULL
import pandas as pd
import pymongo
import datetime
from collections import Counter
import numpy as np

myclient = pymongo.MongoClient("mongodb://root:example@192.168.164.129:27017/")
mydb = myclient["exampledb"]
mycol = mydb["example"]

mydoc = mycol.find({})
data = pd.DataFrame(list(mydoc))

writer = pd.ExcelWriter('data.xlsx', engine='openpyxl')
data.to_excel(writer,sheet_name='data')
writer.save()

writer = pd.ExcelWriter('question.xlsx', engine='openpyxl')

data_count = pd.DataFrame.from_dict(Counter(data['site_id']),orient='index',columns=["Count"])

print('第一題')
q1 =data_count.sort_values(by='Count',ascending=False).head(1)
q2 =data_count.sort_values(by='Count',ascending=True).head(1)

q1.to_excel(writer,sheet_name='1-1')
q2.to_excel(writer,sheet_name='1-2')


data.replace(to_replace=r'^\s*$',value=np.nan,regex=True,inplace=True)
data = data[pd.isnull(data['lic_date'])==False]

data =data.reset_index()
data = data.drop('index', axis=1)

data = data[pd.to_numeric(data['lic_date'].str.split('/').str.get(1))<12]

data = data.reset_index()
data = data.drop('index', axis=1)

data['lic_date'] = (pd.to_numeric(data['lic_date'].str.split('/').str.get(0), downcast='integer')+1911).astype(str)+'-'+data['lic_date'].str.split('/').str.get(1)+'-'+data['lic_date'].str.split('/').str.get(2)


data['lic_date'] = pd.to_datetime(data['lic_date'])

d = datetime.datetime(2020, 11, 1)
data['lic_time']=d-data['lic_date']
dfSort=data.sort_values(by='lic_time',ascending=False).head(1)

print('第二題')
q3 = dfSort['site_id']

q4 = data[data['bui_person']==dfSort['bui_person'].item()]
q5 = data[data['des_person']==dfSort['des_person'].item()]

q3.to_excel(writer,sheet_name='2-1')
q4.to_excel(writer,sheet_name='2-2')
q5.to_excel(writer,sheet_name='2-3')


print('第三題')
data_oldbui = data[data['lic_time'].dt.days>20000]
data_count = pd.DataFrame.from_dict(Counter(data_oldbui['site_id']),orient='index',columns=["Count"])
q6 = data_count
q6.to_excel(writer,sheet_name='3')

writer.save()