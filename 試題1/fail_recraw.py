from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup as BS
import  json, ssl, urllib.request, os, glob, time, base64
from PIL import Image
import pytesseract
import cv2 as cv
import multiprocessing as mp

'''filename'''
captcha = 'captcha_temp.png'
cmp_road_name_file = 'cmp_road_name.data'


'''link'''
target_url='https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3'
road_name_url='https://www.ris.gov.tw/rs-opendata/api/v1/datastore/ODRP049/110'
cmp_road_name_url='https://building-management.publicwork.ntpc.gov.tw/_setData.jsp?rt=D1'

'''xpath'''
t1='/html/body/div[2]/div/div[2]/section[2]/form/div[2]/div[13]/input[2]' #區
t2='/html/body/div[2]/div/div[2]/section[2]/form/div[2]/div[13]/input[4]' #路
t3='/html/body/div[2]/div/div[2]/section[2]/form/div[2]/div[25]/input'    #驗證碼
t4='/html/body/div[2]/div/div[2]/section[2]/form/div[2]/table/tbody/tr/td[2]/button' #查詢按鈕
t5='/html/body/div[1]/center/input' #bypass javascript
t6='/html/body/div[2]/div/div[2]/section[2]/form/div[2]/div[25]/img[1]' #驗證碼圖

u1='/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b/a[3]' #下一頁
u2='/html/body/div/div/div[2]/section[2]/table/tbody[2]/tr/td'     #查無資料
u3='/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b/a[4]' #共x頁

d1="//tbody[@id='DataBlock']/tr/td[1]/a" #
d2="//tbody[@id='DataBlock']/tr/td[2]"   #
d3="//tbody[@id='DataBlock']/tr/td[3]"   #
d4="//tbody[@id='DataBlock']/tr/td[4]"   #
d5="//tbody[@id='DataBlock']/tr/td[5]"   #
d6="//tbody[@id='DataBlock']/tr/td[6]"   #

def get_cmp_road_name(url):
    context = ssl._create_unverified_context()
    with urllib.request.urlopen(url, context=context) as htmldata:
        html = htmldata.read()
    soup = BS(html,'lxml')
    name = soup.find_all('div',{'class':'div_w_85'})
    if name:
        data = [{'site_id':i['onclick'].split('\'')[3],'site_idnum':i['onclick'].split('\'')[1]} for i in name]
    with open(cmp_road_name_file,'w') as f:
        json.dump(data,f)

def get_rnd_pic(driver):
    img_base64 = driver.execute_script("""
        var ele = arguments[0];
        var cnv = document.createElement('canvas');
        cnv.width = ele.width; cnv.height = ele.height;
        cnv.getContext('2d').drawImage(ele, 0, 0);
        return cnv.toDataURL('image/jpeg').substring(22);    
        """, driver.find_element_by_xpath(t6))
    with open(captcha, 'wb') as image:
        image.write(base64.b64decode(img_base64))

def recognize_text(image_name):
    image = cv.imread(image_name)
    gray = cv.cvtColor(image,cv.COLOR_BGR2GRAY)
    ret,binary = cv.threshold(gray,0,255,cv.THRESH_BINARY_INV|cv.THRESH_OTSU)  
    kernel = cv.getStructuringElement(cv.MORPH_RECT,(2,1))
    mid1 = cv.morphologyEx(binary,cv.MORPH_OPEN,kernel)
    kernel = cv.getStructuringElement(cv.MORPH_RECT,(1,2))
    mid1 = cv.morphologyEx(mid1,cv.MORPH_OPEN,kernel)
    #cv.imwrite('test.jpg',  mid1)
 
    contours, hie = cv.findContours(mid1, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    
    cv.drawContours(image, contours, -1, (0, 0, 255), 1)
    #cv.imwrite('test1.jpg', image)
    contours_sort=[cv.boundingRect(cnt) for cnt in contours]
    for cnt in contours_sort:
        if cnt[2] >25:
            contours_sort.remove(cnt)
            contours_sort.append((cnt[0],cnt[1],int(cnt[2]/2),cnt[3]))
            contours_sort.append((cnt[0]+int(cnt[2]/2),cnt[1],int(cnt[2]/2),cnt[3]))
    contours_sort=sorted(contours_sort, key=lambda x:x[0])
    print(contours_sort)
      
    #flag = 1
    textarray=[]
    for cnt in contours_sort:

        top_size,bottom_size,left_size,right_size=(cnt[2],cnt[2],cnt[3],cnt[3]) 
        finalimg=mid1[cnt[1]:cnt[1]+cnt[3], cnt[0]:cnt[0]+cnt[2]]
        
        cv.bitwise_not(finalimg,finalimg)
        constant=cv.copyMakeBorder(finalimg,top_size,bottom_size,left_size,right_size,cv.BORDER_CONSTANT,value=(255,255,255))
        #cv.imwrite('char%s.jpg'%flag, constant)
        textImage = Image.fromarray(constant)   
        text = pytesseract.image_to_string(textImage,lang='eng',config='-c tessedit_char_whitelist=0123456789 --psm 10').strip()
        textarray.append(text)
        #flag += 1

    print(textarray)
    return ''.join(textarray)
def craw_data(driver,site,road):
    print('[*] 正在爬取'+site['site_id']+'_'+road)
    temp=[]
    driver.get(target_url)
    time.sleep(2)
    element0 = driver.find_element_by_xpath(t5)
    element0.click()
    get_rnd_pic(driver)

    text = recognize_text(captcha)
    print(text)
    driver.execute_script('arguments[0].value ='+site['site_idnum']+';', driver.find_element_by_xpath(t1))
    element2 = driver.find_element_by_xpath(t2)
    element2.send_keys(road)
    element3 = driver.find_element_by_xpath(t3)
    element3.send_keys(text)
    element4 = driver.find_element_by_xpath(t4)
    element4.click()

    time.sleep(2)
    if driver.find_element_by_xpath(u2).text=='查無資料':
        driver.close()
        print('[*] 無資料！'+site['site_id']+'_'+road)
        return
    totalcount = driver.find_element_by_xpath(u3).get_attribute('onclick')
    totalcount = totalcount.split('(')[1].split(')')[0]
    for k in range(int(totalcount)):
        datatemp=[]
        for m in [d1,d2,d3,d4,d5,d6]:
            data = driver.find_elements_by_xpath(m)
            datatemp.append([i.text for i in data])
        for l in range(len(datatemp[0])):
            temp.append({'site_id':site['site_id'],'road':road,'use_lic_num':datatemp[0][l],'bui_lic_num':datatemp[1][l],'bui_person':datatemp[2][l],'des_person':datatemp[3][l],'bui_addr':datatemp[4][l],'lic_date':datatemp[5][l]})
        if int(totalcount)>(k+1):
            driver.find_element_by_xpath(u1).click()
            time.sleep(3)
    #print(temp)
    driver.close()
    with open(site['site_id']+'_'+road+'.json','w') as f:
        json.dump(temp,f)
    print('[*] 成功爬取'+site['site_id']+'_'+road)

def except_craw_data(driver,site,road):
    driver.close()
    print('[*] '+site['site_id']+'_'+road+' 爬取失敗！')
    with open(site['site_id']+'_'+road+'.fail','w') as f:
        f.write('fail')
    
def multi_craw(site,road):
    driver = webdriver.Chrome(ChromeDriverManager().install())
    try:
        craw_data(driver,site,road)
    except:
        except_craw_data(driver,site,road)

if __name__ == "__main__":
    #取得新北市區對應編號
    if not os.path.isfile(cmp_road_name_file):
        get_cmp_road_name(cmp_road_name_url)
    with open(cmp_road_name_file,'r') as f:
            cmp_road_name = json.load(f)
    

    
    all_fail = glob.glob("*.fail")
    with open('faillist.data','w') as f:
        json.dump(all_fail,f)
       
    all_fail_list = [i.split('.')[0].split('_') for i in all_fail]  
    for i in all_fail:
        os.remove(i)

    pool = mp.Pool(processes = 2)

    for i in all_fail_list:
        i[0] = [j for j in cmp_road_name if i[0]==j['site_id']][0]
        pool.apply_async(multi_craw, (i[0],i[1], ))                   
    pool.close()
    pool.join()