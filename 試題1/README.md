# 程式執行說明

## 試題1

1. 請先依照requirement.txt安裝環境
1. 執行craw.py，開始爬蟲，爬蟲會開始產生爬蟲成功的json檔案以及意外中斷的fail檔案
1. 執行fail_recraw.py，將可能因驗證碼錯誤或其他導致終止的爬蟲重新執行
1. 執行craw_bulk.py，將爬下來的結果統整上傳至mongodb，mongodbip=192.168.164.129:27017,user=root,pass=example